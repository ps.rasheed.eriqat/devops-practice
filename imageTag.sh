#!/bin/bash -v

#bash script to build and push docker image with desierd tag.

echo `git log --source pom.xml > logs.txt`
log=`cat logs.txt`
commitHash=`git rev-parse --short=8 HEAD`
commitDate=`git log --date=short --pretty=format:"%ad %H"|grep $commitHash`
result=$(echo "$commitDate" |awk '{ print $1}'| cut  -b1-1,2,3,4,6,7,9,10)
echo "$result-$commitHash" 

sudo docker build -t  eriqat/devops-practice:`echo "$result-$commitHash" 
` . 


