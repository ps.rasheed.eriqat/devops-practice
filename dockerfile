FROM openjdk:11-jre-slim
COPY  ./target/assignment-*.jar /usr/local/lib/assignment.jar
EXPOSE 8090
ENTRYPOINT ["java","-jar","-Dspring.profiles.active=h2","/usr/local/lib/assignment.jar"]

