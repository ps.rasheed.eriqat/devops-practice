#!/bin/bash -v

echo `git log --source pom.xml > logs.txt`
log=`cat logs.txt`
commitHash=`git rev-parse --short=8 HEAD`
commitDate=`git log --date=short --pretty=format:"%ad %H"|grep $commitHash`
result=$(echo "$commitDate" |awk '{ print $1}'| cut  -b1-1,2,3,4,6,7,9,10)
version= echo "$result-$commitHash" > version.txt
mvn versions:set -DnewVersion=`cat version.txt`
